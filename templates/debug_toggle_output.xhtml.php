<div id="debug_%DEBUG_ELEMENT_NAME%">
  <span><a href="javascript:toggleDebugContent('debug_%DEBUG_ELEMENT_NAME%_content')">(Show|Hide|Download)</a> %DEBUG_ELEMENT_NAME%</span>
  <div id="debug_%DEBUG_ELEMENT_NAME%_content" style="display:none">
%DEBUG_DATA%
  </div>
</div>
<script type="text/javascript">
   if (typeof toggleDebugContent != 'function') {
      function toggleDebugContent (targetElementID) {
         $("#" + targetElementID).toggle();
      }
   }
</script>
