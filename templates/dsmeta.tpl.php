                  <h2>s4m Dataset Metadata List</h2>
                  <p>All unique dataset metadata name and value pairs are shown here. These results are generated in real-time from Stemformatics Beta database.</p>
                  <div id="dstable_div"></div>
		  <noscript>
		  <div class="warning major">
		    <p>Your browser either has Javascript turned off or is not Javascript-capable. This is a required standard for the service you are attempting to access. Please enable Javascript or upgrade your browser.</p>
		  </div>
		  </noscript>
                  <script type="text/javascript">
                     $(document).ready(function() {
                        var dsmeta = %DSMETA_JSON%;
                        $('#dstable_div').html( '<table cellpadding="1em" cellspacing="0" border="0" class="display" id="dstable"><thead><th style="text-align:left">Name</th><th style="text-align:left">Value</th></thead><tbody id="dstablebody"></tbody></table>' );
                        for (d in dsmeta) {
                           var tuple = dsmeta[d];
                           $("#dstablebody").append('<tr><td>'+tuple['ds_name']+'</td><td>'+tuple['ds_value']+"</td></tr>\n");
                        }
                        $('#dstable').dataTable({
                           "bPaginate": false,
                           "oLanguage": { "sSearch": "Filter" }
                        });
                     });
                  </script>

