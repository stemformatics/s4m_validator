## Overview

This is the PHP-based web validator for stemformatics annotations.

It is being decommissioned in 2018 (validation will be integrated within Stemformatics python source).


## License and contributing

The `s4m_validator` project is Open Source and licensed under the [Apache License](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)), Version 2.0 (see `LICENSE.txt`).

