<?php

   //header('Content-Type: text/html; charset=utf-8');

   include_once 'src/appcustom.php';

   // A few globals
   $_TEMPLATE = 'templates/index.tpl.php';
   $_CONTENT = '';
   $_ERROR = '';

   // Our application object
   $app = new appcustom;
   assert( is_object($app) );

   // First step, deal with any fatal startup errors (e.g. database connection etc.)
   if ($app->getError()) {
       $app->set_token('MESSAGE', $app->error_span($app->getError()));
       $_CONTENT = $app->render_template('content_main');
   // .. else process incoming request (if any)
   } else {
       // returns token-replaced XHTML which becomes "#content" section of site template specified by $_TEMPLATE
       $_CONTENT = $app->handle_request();
   }

   // load site template with template variables replaced
   include($_TEMPLATE);
?>

