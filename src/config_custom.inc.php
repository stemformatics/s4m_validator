<?php

   /**
      File:     config_custom.inc.php
      Synopsis: Custom configuration settings.
      Author:   O.Korn
      Created:  March 2008
   **/


   // Error log file path
   $_ERROR_LOG = '/var/www/html/validator/log/error.log';

   // Activity log file path
   $_ACTIVITY_LOG = '/var/www/html/validator/log/access.log';

   // Base URI of Stemformatics API provider path
   $_S4M_API_URI = 'http://stemformatics.aibn.uq.edu.au/api';

?>
