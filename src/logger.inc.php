<?php
/**
  File:     logger.inc.php
  Synopsis: Class for logging messages to file or other location
  Author:   O.Korn
  Created:  March 2008
**/


/**
   Class:    logger 
   Synopsis: Custom class for message logging.

   NOTE:     Currently only file logging supported, but DB logging may be
             implemented soon.
**/
class logger {

   var $_config;
   var $_error;

   /// Constructor
   function logger ($config = array()) {
      assert( is_array($config) );

      $this->_error = '';

      if (count($config) ) {
         $this->_config = $config;

         foreach ($this->_config as $logID => $logDetailsHash) { 
            if (count($logDetailsHash)) {
               $type = '';
               $location = '';
               foreach ($logDetailsHash as $opt => $val) {
                  if (strtolower($opt) == 'type') {
                     $type = strtolower($val);
                  } elseif (strtolower($opt) == 'location') {
                     $location = strtolower($val);
                  }
               }
               if (strlen($type) and strlen($location)) {
                  if ($type == 'file') {
                     /// check that file is writeable 
                     if (file_exists($location)) {
                        if (! is_writable($location)) {
                           $this->_error = "Could not verify write access to log file '$location' for log resource '$logID'!";
                           return;
                        }
                     /// attempt to create log file
                     } else {
                        $handle = fopen($location, 'a+');
                        if (! $handle) {
                           $this->_error = "Could not verify write access to log file '$location' for log resource '$logID'!";
                           return;
                        }
                        fclose($handle);
                     }
                  /// Only 'file' type logging supported at the moment!
                  } else {
                     $this->_error = "Unsupported log type '$type' for log resource '$logID'!";
                     return;
                  }
               } else {
                  $this->_error = "Log type and/or location could not be determined for log resource '$logID'!";
                  return;
               }
            /// ..something's wrong with the configuration (it's empty)
            } else {
               $this->_error = 'Internal Error (001)';
               return;
            }
         }

      } else {
         $this->_config = null;
         $this->_error = 'Internal Error (002)';
      }
   }

   /// Destructor
   function __destruct () {
   }

   function isError () {
      return (strlen($this->_error) > 0);
   }

   function getError () {
      return $this->_error;
   }

   /// ---------------------------------------------------------------
   /// Log a message to a given log target
   /// ---------------------------------------------------------------
   function log ($logID, $message = '') {
      assert( isset($logID) and strlen($logID) );

      $logDetails = $this->_config[$logID];

      $type = strtolower($logDetails['type']);
      $target = strtolower($logDetails['location']);
      $autotime = strtolower($logDetails['autotime']);
      $autonewline = strtolower($logDetails['autonewline']);
    
      /// Prepend timestamp to message 
      if ($autotime == '1' or $autotime == 'yes' or $autotime == 'true') {
         /// Return server's textual current timestamp in RFC 2822 format
         $ts = date('r');
         $message = "[$ts] $message";
      }
      /// If set, automatically append a new line to message
      if ($autonewline == '1' or $autonewline == 'yes' or $autonewline == 'true') {
         $message .= "\n";
      }
      /// Do file log
      if ($type == 'file') {
         return $this->logToFile($target, $message); 
      /// Only file logging supported at the moment!
      } else {
         $this->_error = "Unsupported log type '$type'!";
      }
      return false; 
   }


   function getConfig () {
      return $this->_config;
   }


   /// Get the 'default' log resource.
   /// If one or more specified, the first one is the default one.
   function getDefaultLogID () {
      if (! is_null($this->_config)) {
         if (count($this->_config) >= 1) {
            $ids = array_keys($this->_config);
            return $ids[0]; 
         }
      }
      return '';
   }


   /*** Private Methods ***/


   /// ---------------------------------------------------------------
   /// Log a message to a file
   /// NOTE: Should *not* call this directly from application code,
   ///       use public log() method instead.
   /// ---------------------------------------------------------------
   function logToFile ($filePath, $text = '') {
      assert( isset($filePath) and strlen($filePath) );

      if (is_writable($filePath)) {
         $handle = fopen($filePath, "a+");
         if ($handle) {
            $nbytes = fwrite($handle, $text);
            if ($nbytes === false) {
               $this->_error = '';
            } else {
               fclose($handle);
               return true;
            }
         } else {
            $this->_error = '';
         }
      } else {
         $this->_error = '';
      }
      return false;
   }

} /// class logger

?>
